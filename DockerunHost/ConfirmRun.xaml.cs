﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Win32;

namespace DockerunHost
{
    /// <summary>
    /// Interaction logic for ConfirmRun.xaml
    /// </summary>
    public partial class ConfirmRun : Window
    {
        private enum OPERATIONAL_MODE
        {
            Run,
            Compose
        }


        private uint remainingSecondsForAllowBtn = 3;
        private DispatcherTimer allowBtnEnableWaitTimer;
        private OPERATIONAL_MODE operationalMode = OPERATIONAL_MODE.Run;
        private string dockerComposePath = null;
        

        public ConfirmRun()
        {
            InitializeComponent();

            try
            {
                // Decode command (can be a run or compose)
                string command =
                    Encoding.UTF8.GetString(
                        Convert.FromBase64String(Environment.GetCommandLineArgs()[1].Replace("dockerun:", "")));

                // If the command is a Docker compose one
                if (command.StartsWith("compose"))
                {
                    operationalMode = OPERATIONAL_MODE.Compose; // Change operational mode
                    command = command.Replace("compose", ""); // Remove compose from command
                    Height = 300d; // Increase window height
                    MinHeight = 300d; // Increase min window height
                    ResizeMode = ResizeMode.CanResizeWithGrip; // Window is resizable
                    descriptionLbl.Content = "Your docker-compose.yml file:";
                    authorizeBtn.Content = "Execute";
                    dockerRunCommandTxtArea.AcceptsReturn = true;

                    denyBtn.Visibility = Visibility.Hidden;
                }
                else if (command.StartsWith("docker run "))
                {
                    composePanel.Visibility = Visibility.Hidden;
                    Width = 464d;
                    MinWidth = 464d;
                }
                else
                {
                    throw new Exception("Not a Docker command");
                }

                dockerRunCommandTxtArea.Text = command; // Writes the command to the textarea

                secondsToWaitLbl.Content = $"Wait {remainingSecondsForAllowBtn} seconds"; // Sets seconds to wait label

                // Configures 1 sec timer and starts it
                allowBtnEnableWaitTimer = new DispatcherTimer();
                allowBtnEnableWaitTimer.Tick += allowBtnEnableWaitTimer_Tick;
                allowBtnEnableWaitTimer.Interval = new TimeSpan(0, 0, 1);
                allowBtnEnableWaitTimer.Start();
            }
            catch (Exception) // If the command is not a base64 valid or a Docker one
            {
                MessageBox.Show("Invalid docker run command", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown(-3);
            }
        }

        private void authorizeBtn_Click(object sender, RoutedEventArgs e)
        {
            string command = null;
            if (operationalMode == OPERATIONAL_MODE.Run)
            {
                command = dockerRunCommandTxtArea.Text;
            }
            else if(operationalMode == OPERATIONAL_MODE.Compose)
            {
                if (dockerComposePath == null)
                {
                    dockerComposePath = Path.Combine(Path.GetTempPath(), "dockerun-compose.yml");
                    File.WriteAllText(dockerComposePath, dockerRunCommandTxtArea.Text);
                }
                command = $"cd /D {Path.GetDirectoryName(dockerComposePath)} && docker-compose -f {Path.GetFileName(dockerComposePath)} up -d";
            }

            if (command != null)
            {
                Process process = new Process();
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = $"/C {command} & timeout 5";
                process.StartInfo = startInfo;
                process.Start();
                Application.Current.Shutdown(0);
            }
            else
            {
                Application.Current.Shutdown(-2);
            }
        }

        private void denyBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown(-1);
        }

        private void allowBtnEnableWaitTimer_Tick(object sender, EventArgs e)
        {
            if (remainingSecondsForAllowBtn > 1)
            {
                remainingSecondsForAllowBtn--;
                secondsToWaitLbl.Content = $"Wait {remainingSecondsForAllowBtn} seconds";
                allowBtnEnableWaitTimer.Start();
            }
            else
            {
                secondsToWaitLbl.Visibility = Visibility.Hidden;
                authorizeBtn.IsEnabled = true;
                saveExecuteBtn.IsEnabled = true;
            }
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Save your docker-compose.yml file";
            dialog.DefaultExt = "yml";
            dialog.Filter = "yml files (*.yml;*yaml)|*.yml;*.yaml|All files (*.*)|*.*";
            dialog.FileName = "docker-compose";

            if (dialog.ShowDialog() == true)
            {
                dockerComposePath = dialog.FileName;
                File.WriteAllText(dockerComposePath, dockerRunCommandTxtArea.Text);
            }
        }

        private void saveExecuteBtn_Click(object sender, RoutedEventArgs e)
        {
            saveBtn_Click(sender, e);
            authorizeBtn_Click(sender, e);
        }
    }
}
